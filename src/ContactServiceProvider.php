<?php

namespace Sahil\Calculator;

use Illuminate\Support\ServiceProvider;

class ContactServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Sahil\Calculator\CalculatorController');
        $this->loadViewsFrom(__DIR__.'/views', 'calculator');
        $this->loadRoutesFrom(__DIR__.'/routes.php');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__.'/routes.php';
        $this->publishes([
            __DIR__.'/views' => base_path('resources/views/sahil/calculator'),
        ]);

    }
}
